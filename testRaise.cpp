#include <iostream>
#include <string>

int m_count = 0;
int m_nbFailed = 0;


// remarque: mingw ne supporte pas std::to_string - à tester sous linux
///*

#define assertRaise(desc, func, e) {int _line = __LINE__;					\
		m_count++;									\
		try { 										\
			func;									\
			_raiseFailed(_line, desc, "nothing", std::to_string(e));		\
		}										\
		catch (int i) {									\
			if (i != e)								\
				_raiseFailed(_line, desc, std::to_string(i), std::to_string(e));\
		}										\
	}

//*/

#define assertNotRaise(desc, func, e) { int _line = __LINE__; 	\
		m_count++;																	\
		try { 																			\
			func; 																		\
		}																					\
		catch (int i) {																	\
			if (i == e) 																	\
				_notRaiseFailed(_line, desc, std::to_string(e)); 								\
		}																					\
	}

#define assertAnyRaise(desc, func) { int _line = __LINE__;		\
		m_count++;																	\
		try { 																			\
			func;																			\
			_raiseFailed(_line, desc, "nothing", "something");									\
		}																					\
		catch (int i) {																	\
		}																					\
	}
	
#define assertNoRaise(desc, func) { int _line = __LINE__; 		\
		m_count++;																	\
		try { 																			\
			func; 																		\
		}																					\
		catch (int i) {																	\
			_notRaiseFailed(_line, desc, "anything"); 									\
		}																					\
	}

void print_fail(int line, std::string desc, std::string val, std::string val_comp)
{
	std::cout << "test ['" << desc << "' @ l" << line << "] failed : expected " << val_comp << ", got " << val << std::endl;
}

void _raiseFailed (int line, std::string desc, std::string val, std::string val_comp)
{
	m_nbFailed++;
	print_fail(line, desc, val, val_comp);
}

void _notRaiseFailed(int line, std::string desc, std::string val_comp)
{
	m_nbFailed++;
	std::cout << "test ['" << desc << "' @ l" << line << "] failed : didn't expect " << val_comp << std::endl ;
}

void test(int n)
{
	if (n == 0 || n == 1)
		throw n;
}
	
int main()
{
	assertRaise("test 0", test(0), 0);
	assertRaise("test 1", test(1), 0);
	assertRaise("test 2", test(2), 0);
	
	assertNotRaise("test not 0,0", test(0), 0);
	assertNotRaise("test not 0,1", test(0), 1);
	assertNotRaise("test not 0,2", test(0), 2);
	
	assertAnyRaise("test any 1", test(1));
	assertAnyRaise("test any 2", test(2));
	
	assertNoRaise("test no 1", test(1));
	assertNoRaise("test no 2", test(2));
	
	assertNoRaise("test line 0", {int i=0; if (i==0) throw 0;});
	assertNoRaise("test line 1", {int i=0; if (i==1) throw 0;});
	
	// fonctions ^ ne pourront pas être des membres de UnitTest -> assertRaise(UnitTest &test, ...) en réalité (prévoir UnitTest::incCount() & incNbFail())
	
	std::cout << "done (failed " << m_nbFailed << "/" << m_count << ")" << std::endl;
	
	/*
	test ['test 1' @ l109] failed : expected 0, got 1
	test ['test 2' @ l110] failed : expected 0, got nothing
	test ['test not 0' @ l112] failed : didn't expect 0
	test ['test any 2' @ l117] failed : expected something, got nothing
	test ['test no 1' @ l119] failed : didn't expect anything
	test ['test line 0' @ l122] failed : didn't expect anything
	done (failed 6/12)
	*/
	
	return 0;
}

