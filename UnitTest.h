#ifndef UNITTEST_H
#define UNITTEST_H

class UnitTest {
	private:
		int m_count;
		int m_nbFailed;
		bool m_exitOnFail;
		
		std::string uniform(const std::string desc);
		void print_fail(int line, std::string desc, std::string val_exp, std::string symbol, std::string val_got);
		void evalTest(int line, std::string desc, std::string val_exp, std::string symbol, std::string val_got, bool test);
		//bool evalTest(bool test);
	
	public:
		UnitTest();
		
		void _assertEqual		(int line, std::string desc, std::string val_exp, std::string val_got); 
		void _assertDifferentThan	(int line, std::string desc, std::string val_exp, std::string val_got);
		
		void _assertEqual		(int line, std::string desc, int val_exp, int val_got);
		void _assertDifferentThan	(int line, std::string desc, int val_exp, int val_got);
		void _assertLessThan		(int line, std::string desc, int val_exp, int val_got);
		void _assertLessOrEqualThan	(int line, std::string desc, int val_exp, int val_got);
		void _assertMoreThan		(int line, std::string desc, int val_exp, int val_got);
		void _assertMoreOrEqualThan	(int line, std::string desc, int val_exp, int val_got);
				
		void _assertTrue		(int line, std::string desc, bool val);
		void _assertFalse		(int line, std::string desc, bool val);
				
		void _raiseFailed 		(int line, std::string desc, std::string val_exp, std::string val_got);
		void _notRaiseFailed		(int line, std::string desc, std::string val_got);
				
		//bool getEqual							(std::string val, std::string val_comp);	// ?
							
		void reset();
				
		int getCount();
		int getNbFailed();

		void incCount();
		
		void setExitOnFail(bool exitOnFail);
		
		void printCCL();
};

// TODO inv val_exp & val_got
#define assertEqual(desc, val_exp, val_got) _assertEqual(__LINE__, desc, val_exp, val_got)
#define assertDifferentThan(desc, val_exp, val_got) _assertDifferentThan(__LINE__, desc, val_exp, val_got)
#define assertLessThan(desc, val_exp, val_got) _assertLessThan(__LINE__, desc, val_exp, val_got)
#define assertLessOrEqualThan(desc, val_exp, val_got) _assertLessOrEqualThan(__LINE__, desc, val_exp, val_got)
#define assertMoreThan(desc, val_exp, val_got) _assertMoreThan(__LINE__, desc, val_exp, val_got)
#define assertMoreOrEqualThan(desc, val_exp, val_got) _assertMoreOrEqualThan(__LINE__, desc, val_exp, val_got)
#define assertTrue(desc, val) _assertTrue(__LINE__, desc, val)
#define assertFalse(desc, val) _assertFalse(__LINE__, desc, val)

#define assertRaise(test, desc, func, e) {int _line = __LINE__;						\
		test.incCount();									\
		try { 											\
			func;										\
			test._raiseFailed(_line, desc, std::to_string(e), "nothing");			\
		}											\
		catch (int i) {										\
			if (i != e)									\
				test._raiseFailed(_line, desc, std::to_string(e), std::to_string(i));	\
		}											\
	}

#define assertNotRaise(test, desc, func, e) { int _line = __LINE__; 					\
		test.incCount();									\
		try { 											\
			func; 										\
		}											\
		catch (int i) {										\
			if (i == e) 									\
				test._notRaiseFailed(_line, desc, std::to_string(e)); 			\
		}											\
	}

#define assertAnyRaise(test, desc, func) { int _line = __LINE__;					\
		test.incCount();									\
		try { 											\
			func;										\
			test._raiseFailed(_line, desc, "nothing", "something");				\
		}											\
		catch (int i) {										\
		}											\
	}
	
#define assertNoRaise(test, desc, func) { int _line = __LINE__; 					\
		test.incCount();									\
		try { 											\
			func; 										\
		}											\
		catch (int i) {										\
			test._notRaiseFailed(_line, desc, "anything"); 					\
		}											\
	}

#endif

