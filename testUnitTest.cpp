#include <iostream>
#include "UnitTest.h"

void func_test(int n)
{
	if (n == 0 || n == 1)
		throw n;
}

int main()
{
	UnitTest test;
	test.setExitOnFail(false);
	
	test.assertEqual("string 1", "hello", "hello");
	test.assertEqual("string 2", "hello", "goodbye");									// false
	
	test.assertDifferentThan("string 3", "hello", "hello");					// false
	test.assertDifferentThan("string 4", "hello", "goodbye");
	
	test.assertEqual("int 1", 1, 52);															// false
	test.assertEqual("int 2", 52, 52);
	
	test.assertDifferentThan("int 3", 1, 52);										
	test.assertDifferentThan("!= int 4", 52, 52);														// false
	
	test.assertLessThan("int 5", 1, 52);										
	test.assertLessThan("int 6", 52, 52);																// false
	
	test.assertLessOrEqualThan("int 7", 12, 52);										
	test.assertLessOrEqualThan("int 8", 53, 52);												// false
	
	test.assertMoreThan("int 9", 12, 52);															// false
	test.assertMoreThan("int 10", 2, 1);															
	
	test.assertMoreOrEqualThan("int 11", 12, 52);												// false
	test.assertMoreOrEqualThan("int 12", 52, 52);												
	
	test.assertTrue("true 1", true);
	test.assertTrue("true 2", false);																		// false
	
	test.assertFalse("false 1", true);																	// false
	test.assertFalse("false 2", false);
	
	assertRaise(test, "raise 0", func_test(0), 0);
	assertRaise(test, "raise 1", func_test(1), 0);
	assertRaise(test, "raise 2", func_test(2), 0);

	assertNotRaise(test, "raise not 0,0", func_test(0), 0);
	assertNotRaise(test, "raise not 0,1", func_test(0), 1);
	assertNotRaise(test, "raise not 0,2", func_test(0), 2);
	
	assertAnyRaise(test, "raise any 1", func_test(1));
	assertAnyRaise(test, "raise any 2", func_test(2));
	
	assertNoRaise(test, "raise no 1", func_test(1));
	assertNoRaise(test, "raise no 2", func_test(2));
	
	assertNoRaise(test, "raise line 0", {int i=0; if (i==0) throw 0;});
	assertNoRaise(test, "raise line 1", {int i=0; if (i==1) throw 0;});

	test.printCCL();
	std::cout << "count : " << test.getCount() << " (20), failed : " << test.getNbFailed() << " (10)" << std::endl;
	
	test.reset();
	test.printCCL();
	
	test.setExitOnFail(true);
	test.assertEqual("string 1", "hello", "hello");
	test.assertEqual("string 2", "hello", "goodbye");									// false
		
	std::cout << "this statement shouldn't be reached." << std::endl;
	return 0;
}


