#include <iostream>
#include <string>
#include "UnitTest.h"

UnitTest::UnitTest()
{
	m_exitOnFail = true;
	reset();
}

void UnitTest::print_fail(int line, std::string desc, std::string val_exp, std::string symbol, std::string val_got)
{
	std::cout << "[TEST " << ((m_count < 10) ? "0" : "") << m_count << " @ l" << line << " '" << desc << "' ] failed (" << val_exp << symbol << val_got << ")" << std::endl;
}

void UnitTest::evalTest(int line, std::string desc, std::string val_exp, std::string symbol, std::string val_got, bool test)
{
	m_count++;
	if (!test)
	{
		print_fail(line, desc, val_exp, symbol, val_got);
		m_nbFailed++;
		if (m_exitOnFail)
		{
			printCCL();
			exit(1);
		}
	}
}

/*bool UnitTest::evalTest(bool test)
{
	m_count++;
	if (!test)
		m_nbFailed++;
	return test;
}*/

std::string UnitTest::uniform(const std::string desc)
{
	return desc.empty() ? "" : "' '"+desc;
}

void UnitTest::_assertEqual(int line, std::string desc, std::string val_exp, std::string val_got)
{
	evalTest(line, "=="+uniform(desc), val_exp, " != ", val_got, val_exp.compare(val_got) == 0);
}

void UnitTest::_assertDifferentThan(int line, std::string desc, std::string val_exp, std::string val_got)
{
	evalTest(line, "!="+uniform(desc), val_exp, " == ", val_got, val_exp.compare(val_got) != 0);
}

void UnitTest::_assertEqual(int line, std::string desc, int val_exp, int val_got)
{
	evalTest(line, "=="+uniform(desc), std::to_string(val_exp), " != ", std::to_string(val_got), val_exp == val_got);
}

void UnitTest::_assertDifferentThan(int line, std::string desc, int val_exp, int val_got)
{
	evalTest(line, "!="+uniform(desc), std::to_string(val_exp), " == ", std::to_string(val_got), val_exp != val_got);
}

void UnitTest::_assertLessThan(int line, std::string desc, int val_exp, int val_got)
{
	evalTest(line, " <"+uniform(desc), std::to_string(val_exp), " >= ", std::to_string(val_got), val_exp < val_got);
}

void UnitTest::_assertLessOrEqualThan(int line, std::string desc, int val_exp, int val_got)
{
	evalTest(line, "<="+uniform(desc), std::to_string(val_exp), " > ", std::to_string(val_got), val_exp <= val_got);
}

void UnitTest::_assertMoreThan(int line, std::string desc, int val_exp, int val_got)
{
	evalTest(line, " >"+uniform(desc), std::to_string(val_exp), " <= ", std::to_string(val_got), val_exp > val_got);
}
		
void UnitTest::_assertMoreOrEqualThan(int line, std::string desc, int val_exp, int val_got)
{
	evalTest(line, ">="+uniform(desc), std::to_string(val_exp), " < ", std::to_string(val_got), val_exp >= val_got);
}

void UnitTest::_assertTrue(int line, std::string desc, bool val)
{
	evalTest(line, " T"+uniform(desc), "expected true", ", ", "got false", val);
}

void UnitTest::_assertFalse(int line, std::string desc, bool val)
{
	evalTest(line, " F"+uniform(desc), "expected false", ", ", "got true", !val);
}

/*bool UnitTest::getEqual(std::string val, std::string val_got)
{
	return evalTest(line, val.equals(val_got));
}*/

void UnitTest::_raiseFailed (int line, std::string desc, std::string val_exp, std::string val_got)
{
	m_nbFailed++;
	print_fail(line, " R"+uniform(desc), "expected "+val_exp, ", ", "got "+val_got);
	if (m_exitOnFail)
	{
		printCCL();
		exit(1);
	}
}

void UnitTest::_notRaiseFailed(int line, std::string desc, std::string val_got)
{
	m_nbFailed++;
	print_fail(line, "nR"+uniform(desc), "didn't expect ", "", val_got);
	if (m_exitOnFail)
	{
		printCCL();
		exit(1);
	}
}

void UnitTest::reset()
{
	m_count = 0;
	m_nbFailed = 0;
}

int UnitTest::getCount()
{
	return m_count;
}

int UnitTest::getNbFailed()
{
	return m_nbFailed;
}

void UnitTest::incCount()
{
	m_count++;
}

void UnitTest::setExitOnFail(bool exitOnFail)
{
	m_exitOnFail = exitOnFail;
}

void UnitTest::printCCL()
{
	std::cout << m_nbFailed << "/" << m_count << " test(s) failed (" << ((m_count > 0) ? ((float)(m_count-m_nbFailed)*100/m_count) : 100) << "% success)." << std::endl;
}




