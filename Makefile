CFLAGS = -g -Wall --std=c++11
OUTPUT = testUnitTest.out
SRC = testUnitTest.cpp UnitTest.cpp
OBJ = testUnitTest.o UnitTest.o
COMPIL = g++

Debug : testUnitTest.o
	$(COMPIL) $(OBJ) -o $(OUTPUT) $(CFLAGS)
testUnitTest.o : $(SRC)
	$(COMPIL) -c $(SRC) $(CFLAGS)
static :
	ar rcs libUnitTest.a UnitTest.o
clean :
	rm -f *.o
	rm -f *~

